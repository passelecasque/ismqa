# isMQA

Simple tool to identify MQA flac files. 

## Usage

To check a single file: 
```shell
$ isMQA suspicious_file.flac
```
To check a pattern:
```shell
$ isMQA suspicious_*.flac
```
To recursively check a folder:
```shell
$ isMQA suspicious_folder/
```
By default, `isMQA` uses 12 threads and only displays any MQA file it finds. 

To show every file with a marker for MQA files:
```shell
$ isMQA -verbose suspicious_folder/
```
To use a different number of threads:
```shell
$ isMQA -j 1 suspicious_folder/
```
Incidentally, `-j 1` will keep tracks in order in the output, at the cost of time. Useful, but remember time is our most precious of possessions.

## Installation
`isMQA` is a simple binary that requires no configuration. Download it from the artifacts zip in the [tags page of this repository](https://gitlab.com/passelecasque/ismqa/-/tags). 

![instructions](https://i.imgur.com/4hdZQv9.png)

Binaries for Linux, MacOS ("darwin") and Windows are automatically generated. Only the Linux binary is tested.