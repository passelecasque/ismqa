GO = GO111MODULE=on go
VERSION=`git describe --tags`

all: deps build

deps:
	${GO} mod download

clean:
	rm -f isMQA
	rm -f isMQA_windows.exe
	rm -f isMQA_darwin

build:
	${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o isMQA
	GOOS=windows GOARCH=amd64 ${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o isMQA_windows.exe
	GOOS=darwin GOARCH=amd64 ${GO} build -trimpath -ldflags "-X main.Version=${VERSION}" -o isMQA_darwin

install:
	${GO} install -trimpath -ldflags "-X main.Version=${VERSION}"




