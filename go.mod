module gitlab.com/passelecasque/ismqa

go 1.20

require (
	github.com/charmbracelet/bubbles v0.15.0
	github.com/charmbracelet/bubbletea v0.23.2
	github.com/charmbracelet/lipgloss v0.6.0
	gitlab.com/catastrophic/assistance v0.43.1
)

require (
	github.com/aymanbagabas/go-osc52 v1.2.1 // indirect
	github.com/charmbracelet/harmonica v0.2.0 // indirect
	github.com/containerd/console v1.0.3 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/go-flac/flacpicture v0.2.0 // indirect
	github.com/go-flac/go-flac v0.3.1 // indirect
	github.com/icza/bitio v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/mattn/go-localereader v0.0.1 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/mewkiz/flac v1.0.6 // indirect
	github.com/mewkiz/pkg v0.0.0-20190919212034-518ade7978e2 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	github.com/mozillazg/go-unidecode v0.1.1 // indirect
	github.com/muesli/ansi v0.0.0-20211018074035-2e021307bc4b // indirect
	github.com/muesli/cancelreader v0.2.2 // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.14.0 // indirect
	github.com/nsf/termbox-go v0.0.0-20190104133558-0938b5187e61 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/tj/go-spin v1.1.0 // indirect
	gitlab.com/catastrophic/gotabulate v0.0.0-20190228104527-d3d77fbbb3a1 // indirect
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
)
