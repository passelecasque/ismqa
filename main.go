package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/charmbracelet/bubbles/progress"
	"github.com/charmbracelet/bubbles/spinner"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/catastrophic/assistance/flac"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/logthis"
)

type result struct {
	path  string
	err   error
	isMQA bool
}

type model struct {
	flacFiles   []string
	mqaFiles    []string
	index       int
	width       int
	height      int
	spinner     spinner.Model
	progress    progress.Model
	done        bool
	resultsChan chan result
	lastResult  *result
	verbose     bool
}

var (
	checkMark     = lipgloss.NewStyle().Foreground(lipgloss.Color("42")).SetString("✓")
	checkMarkFail = lipgloss.NewStyle().Foreground(lipgloss.Color("9")).SetString("x")
	errorStyle    = lipgloss.NewStyle().Foreground(lipgloss.Color("9"))
)

func newModel(paths []string, resultsChan chan result, verbose bool) model {
	p := progress.New(
		progress.WithDefaultScaledGradient(),
	)
	s := spinner.New()
	s.Style = lipgloss.NewStyle().Foreground(lipgloss.Color("202"))
	return model{
		flacFiles:   paths,
		mqaFiles:    make([]string, len(paths)),
		spinner:     s,
		progress:    p,
		resultsChan: resultsChan,
		lastResult:  &result{},
		verbose:     verbose,
	}
}

func (m model) Init() tea.Cmd {
	return tea.Batch(tea.Printf("Checking flacs for MQA:\n"), m.checkNextFlacFile(), m.spinner.Tick)
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.WindowSizeMsg:
		m.width, m.height = msg.Width, msg.Height
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "esc", "q":
			return m, tea.Quit
		}
	case result:
		if m.index >= len(m.flacFiles)-1 {
			m.done = true
			return m, tea.Quit
		}

		m.index++
		return m, tea.Batch(
			m.progress.SetPercent(float64(m.index)/float64(len(m.flacFiles))),
			m.printOrTick(msg),
			m.checkNextFlacFile(),
		)
	case spinner.TickMsg:
		var cmd tea.Cmd
		m.spinner, cmd = m.spinner.Update(msg)
		return m, cmd
	case progress.FrameMsg:
		progressModel, cmd := m.progress.Update(msg)
		m.progress = progressModel.(progress.Model)
		return m, cmd
	}
	return m, nil
}

func (m model) printOrTick(msg result) tea.Cmd {
	if msg.isMQA || m.verbose {
		var errorIfRelevant string
		if msg.isMQA {
			errorIfRelevant = msg.err.Error()
		}
		return tea.Printf("%s %s %s", m.checkMarkForResult(msg.isMQA), msg.path, errorStyle.SetString(errorIfRelevant))
	}
	return m.spinner.Tick
}

func (m model) checkMarkForResult(isMQA bool) string {
	if !isMQA {
		return checkMark.String()
	}
	return checkMarkFail.String()
}

func (m model) View() string {
	n := len(m.flacFiles)
	var numMQAFiles int
	for _, f := range m.mqaFiles {
		if f != "" {
			numMQAFiles++
		}
	}
	w := lipgloss.Width(fmt.Sprintf("%d", n))
	flacCount := fmt.Sprintf(" %*d/%*d", w, m.index+1, w, n)

	spin := m.spinner.View() + " "
	prog := m.progress.View()

	cellsAvail := max(0, m.width-lipgloss.Width(spin+prog+flacCount))

	info := lipgloss.NewStyle().MaxWidth(cellsAvail).Render("Checking...")

	cellsRemaining := max(0, m.width-lipgloss.Width(spin+info+prog+flacCount))
	gap := strings.Repeat(" ", cellsRemaining)

	if m.done {
		var lastRes string
		if m.lastResult.isMQA || m.verbose {
			var errorIfRelevant string
			if m.lastResult.isMQA {
				errorIfRelevant = m.lastResult.err.Error()
			}
			lastRes = fmt.Sprintf("%s %s %s", m.checkMarkForResult(m.lastResult.isMQA), m.lastResult.path, errorStyle.SetString(errorIfRelevant))
		}
		return lastRes + fmt.Sprintf("\n\nDone! Checked %d flac file(s) and found %d MQA file(s).\n", n, numMQAFiles)
	}

	return spin + info + gap + prog + flacCount
}

func (m model) checkNextFlacFile() tea.Cmd {
	var result result
	select {
	case result = <-m.resultsChan:
		m.lastResult.isMQA = result.isMQA
		m.lastResult.path = result.path
		m.lastResult.err = result.err

		if result.isMQA {
			m.mqaFiles[m.index] = result.path
		}
		d := time.Millisecond * 50
		return tea.Tick(d, func(t time.Time) tea.Msg {
			return result
		})

	case <-time.After(10 * time.Millisecond):
		return tea.Batch(m.checkNextFlacFile(), m.spinner.Tick)
	}
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

var Version = "dev"

func main() {
	numWorkers := flag.Int("j", runtime.NumCPU(), "number of threads")
	verbose := flag.Bool("verbose", false, "verbose mode")
	showVersion := flag.Bool("v", false, "show version")
	flag.Parse()

	if *showVersion {
		fmt.Printf("isMQA v%s\n", Version)
	}

	flacPaths := flag.Args()
	if len(flacPaths) == 0 {
		logthis.Error(errors.New("no argument"), logthis.NORMAL)
		os.Exit(-1)
	}

	fmt.Println("Identifying flac files...")

	var trueFlacs []string
	for _, f := range flacPaths {
		if fs.DirExists(f) {
			flacs, err := fs.GetFilesByExt(f, ".flac")
			if err != nil {
				logthis.Error(err, logthis.NORMAL)
				continue
			}
			trueFlacs = append(trueFlacs, flacs...)
		} else if strings.ToLower(filepath.Ext(f)) == ".flac" {
			trueFlacs = append(trueFlacs, f)
		}
	}

	if len(trueFlacs) == 0 {
		fmt.Println("No flac file found.")
		os.Exit(0)
	}

	resultsChan := make(chan result, 20)
	jobs := make(chan string, len(trueFlacs))

	// launching workers
	for w := 1; w <= *numWorkers; w++ {
		go worker(jobs, resultsChan)
	}
	// sending path for analysis
	for _, p := range trueFlacs {
		jobs <- p
	}
	close(jobs)
	// launching interface to catch & display the results
	if _, err := tea.NewProgram(newModel(trueFlacs, resultsChan, *verbose)).Run(); err != nil {
		fmt.Println("Error running program:", err)
		os.Exit(1)
	}
}

func worker(jobs <-chan string, results chan<- result) {
	for j := range jobs {
		results <- analyzeMQA(j)
	}
}

func analyzeMQA(path string) result {
	r := result{path: path}
	fl, err := flac.New(path)
	if err != nil {
		r.err = err
		return r
	}
	if fl.CheckForMQAMetadata() {
		r.isMQA = true
		r.err = errors.New("-> MQA (metadata)")
	}
	sw, sr, err := fl.CheckForMQASyncword()
	if err != nil {
		r.err = errors.New("error looking for MQA syncwords")
		return r
	}
	if sw {
		r.isMQA = true
		r.err = fmt.Errorf("-> MQA %.1fkHz (syncwords)", float32(sr)/1000.0)
	}
	return r
}
